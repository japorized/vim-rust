set encoding=utf-8
set laststatus=2
set runtimepath=$RUSTVIM_CONFIG_HOME,/usr/share/nvim/runtime,/usr/share/nvim/runtime/pack/dist/opt/matchit,$RUSTVIM_CONFIG_HOME/after/
set packpath=$RUSTVIM_CONFIG_HOME/pack,/usr/share/nvim/runtime,/usr/share/nvim/runtime/pack/dist/opt/matchit
set shada='20,<1000,s100,:0,n$XDG_DATA_HOME/rust-vim/shada/main.shada
set dir=$XDG_DATA_HOME/rust-vim/swap
let &packpath = &runtimepath
set concealcursor=""
set conceallevel=2
set expandtab
set shortmess+=c
set noshowmode
set ignorecase    " Ignore case when searching
set numberwidth=2
set showmatch
set shiftwidth=2
set smartcase
set tabstop=2
set softtabstop=2
set t_Co=256
set timeoutlen=200
set splitbelow
set signcolumn=yes
set sessionoptions="buffers,curdir,tabpages,winsize,winpos"
set splitright
set foldmethod=marker
map <SPACE> <Nop>
let mapleader=" "

filetype plugin indent on
syntax on

let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python'

" netrw Configs
let g:netrw_liststyle=3    " Tree view
let g:netrw_banner=0       " Hide Banner by default, use I to toggle
let g:netrw_browse_split=3 " Opens new file in a new tab
let g:netrw_winsize=25     " netrw to only occupy 25% of the screen
let g:netrw_altv=1

let g:multi_cursor_exit_from_insert_mode=0
let g:multi_cursor_select_all_word_key = '<M-n>'

tnoremap <Esc> <C-\><C-n>

let g:coc_config_home = $RUSTVIM_CONFIG_HOME
let g:coc_data_home = $XDG_DATA_HOME . "/rust-vim/coc"

let g:windowswap_map_keys = 0 "prevent default bindings

colorscheme gruvboxDarkMediumLike
