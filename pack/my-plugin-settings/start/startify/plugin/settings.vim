"            _      _          
"   o._ _  _|__ ._ |_)    __|_ 
" \/|| | |  |(_)|  | \|_|_> |_ 

let g:ascii_header = [
\ '           _      _          ',
\ '  o._ _  _|__ ._ |_)    __|_ ',
\ '\/|| | |  |(_)|  | \|_|_> |_ ',
\ ]
                                                          
let g:startify_custom_header = 'map(g:ascii_header, "\"   \".v:val")'
let g:startify_enable_special = 0
let g:startify_lists = [
      \ { 'type': 'sessions',  'header': ['   Sessions']       },
      \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
      \ { 'type': 'files',     'header': ['   MRU']            },
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ { 'type': 'commands',  'header': ['   Commands']       },
      \ ]
let g:startify_commands = [
    \ {'L': ['Lazygit', 'terminal lazygit']},
    \ ]
let g:startify_session_dir = '~/.data/rust-vim/session'

function! Lazygit() abort
  silent! normal! ":terminal lazygit<CR>a"
endfunction
